<?php namespace Config;

class Autoload
{
	public static function run(){
		spl_autoload_register(function($class){
			$ruta = str_replace("\\","/",$class) . ".php";
			//Rutas de los archivos
			//print $ruta;
			//Incluimos la ruta de los archivos
			include_once $ruta;
		});
	}
}
?>