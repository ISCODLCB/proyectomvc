<?php namespace Config;
/**
* 
*/
class Request
{
	private $controlador;
	private $metodo;
	private $argumento;
	
	function __construct()
	{
		//validmos si existe la variable url del .htaccess
		if (isset($_GET['url'])) {
			//Creamos una ruta filtramos la url
			$ruta = filter_input(INPUT_GET,'url',FILTER_SANITIZE_URL);
			$ruta = explode("/", $ruta);
			$ruta = array_filter($ruta);
			//print_r($ruta);
			

			//Como cargar un controlador por defecto
			if ($ruta[0] == "index.php") {
				$this->controlador = "estudiantes";
			}else{

			//asignamos a las variables
				$this->controlador = strtolower(array_shift($ruta));
			}

//asignamos a las variables su respectivo nombre de la ruta
			$this->metodo = strtolower(array_shift($ruta));
			
			//Si no hay metodo que carque index por defecto
			if (!$this->metodo) {

				$this->metodo = "index";

			}
			//Se queda con el resto
			$this->argumento = $ruta;
			//Checamos si se le asigno 
			//print $this->metodo;
			//Si no lee ninguna ruta esta sera el controlador por defecto y el metodo
		}else{
			//Controlador por defecto y un metodo
			$this->controlador = "estudiantes";
			$this->metodo = "index";
		}
	}

	public function getControlador(){
		return $this->controlador;
	}

	public function getMetodo(){
		return $this->metodo;
	}

	public function getArgumento(){
		return $this->argumento;
	}
}

?>