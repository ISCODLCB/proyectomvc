<?php namespace Config;

/**
 * 
 */
//Archivo tiende a llamarse bootstrap
class Enrutador
{

	public static function run(Request $request){
		$controlador = $request->getControlador() . "Controller";
		//A la ruta le falta un slahs
		$ruta = ROOT . "Controllers" . DS . $controlador . ".php";
		//Saca toda la ruta completa de los contrladores
		//print $ruta;
		$metodo = $request->getMetodo();
        
        //Aqui caida index.php no en el controlador en el metodo
		if ($metodo == "index.php") {
			$metodo = "index";
		}
        //asigna el argumento
		$argumento = $request->getArgumento();

		if (is_readable($ruta)) {
			require_once $ruta;
			$mostrar = "Controllers\\" . $controlador;
			$controlador = new $mostrar;

			if (!isset($argumento)) {
				//retirnar los datos que tiene esta funcion
				//El señor de la vista regresa estos datos de listar
				$datos = call_user_func(array($controlador, $metodo));
			}else{
				$datos = call_user_func_array(array($controlador, $metodo),$argumento);
			}
		}

		//Cargar vista
		$ruta = ROOT . "Views" . DS . $request->getControlador() . DS . $request->getMetodo() . ".php";
		//print $ruta;
		//Validar si se puede leer
		if (is_readable($ruta)) {
			require_once $ruta;
		}else{
			print "No se encpontro la ruta";
		}
	}
} 

?>