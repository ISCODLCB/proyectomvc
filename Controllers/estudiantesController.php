<?php namespace Controllers;
/**
 * 
 */
//LLamamos de model l aclase estudiate y ponemos un alias
use Models\Estudiante as Estudiante;
use Models\Seccion as Seccion;

class estudiantesController
{

//creamos la variable para instanciar la clase
	private $estudiante;
	private $seccion;

	public function __construct(){
	//INstanciamos
		$this->estudiante = new Estudiante();
		$this->seccion = new Seccion();
	}

//metodo index va a listar los estudiantes
	public function index(){
 		//print "Hola soy el index del estudiente";
 		//variable datos guardamos el listado
		$datos = $this->estudiante->listar();
		return $datos;
	}

	public function agregar(){
		if (!$_POST) {
			//si no existe envio post que liste las secciones
			$datos = $this->seccion->listar();
			return $datos;
		}else{
			//permitir imagenagenes
			$permitir = array("image/jpeg","image/png","image/gif","image/jpg");
			$limite = 700;
			if (in_array($_FILES['imagen']['type'] , $permitir) && $_FILES['imagen']['size'] <= $limite * 1024) {
				//print "Hola si funciono";
				//Concatenamos los minutos y segundos al nombre de la imagen para que no halla repetidos
				$nombre = date('i-s') . $_FILES['imagen']['name'];
				//print $nombre;
				$ruta = "Views" . DS . "template" . DS . "imagenes" . DS . "avatars" . DS . $nombre;
				//print $ruta;
				//subir los archivos
				move_uploaded_file($_FILES['imagen']['tmp_name'], $ruta);
				$this->estudiante->set("nombre", $_POST['nombre']);
				$this->estudiante->set("edad", $_POST['edad']);
				$this->estudiante->set("promedio", $_POST['promedio']);
				$this->estudiante->set("imagen", $nombre);
				$this->estudiante->set("id_secciones", $_POST['id_secciones']);
				$this->estudiante->add();
				//nos vamos a la pagina principal
				header("location: " . URL . "estudiantes");
			}
		}
	}

	public function editar($id){
		if (!$_POST) {
			$this->estudiante->set("id", $id);
			$datos = $this->estudiante->view();
		/*$datosSec = $this->seccion->listar();
		$datos = array(

			$datosEst,
			$datosSec

			);*/
			return $datos;
		}else{

			$this->estudiante->set("id", $_POST['id']);
			$this->estudiante->set("nombre", $_POST['nombre']);
			$this->estudiante->set("edad", $_POST['edad']);
			$this->estudiante->set("promedio", $_POST['promedio']);
			$this->estudiante->set("id_secciones", $_POST['id_secciones']);
			$this->estudiante->edit();
			header("location: " . URL . "estudiantes");
		}

	}

	public function listarSecciones(){
		$datos = $this->seccion->listar();
		return $datos;
	}

	public function ver($id){
		$this->estudiante->set("id",$id);
		$datos = $this->estudiante->view(); 
		return $datos;
	}

	public function eliminar($id){
		$this->estudiante->set("id",$id);
		$this->estudiante->delete(); 
		header("location: " . URL . "estudiantes");
	}

} 
 	//instanciamos esta clase
$estudiantes = new estudiantesController();

?>