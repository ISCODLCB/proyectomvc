<?php namespace Models;

/**
* 
*/
class Estudiante
{
	private $id;
	private $nombre;
	private $edad;
	private $promedio;
	private $imagen;
	private $id_secciones;
	private $fecha;
	private $con;

	//Autoload no required

	public function __construct(){
		$this->con = new Conexion();
	}

	public function set($atributo,$contenido){
		$this->$atributo = $contenido;
	}

	public function get($atributo){
		return $this->$atributo;
	}

	public function listar(){
		$sql = "SELECT t1.*, t2.nombre as nombre_seccion from estudiantes t1 INNER JOIN secciones t2 ON t1.id_secciones = t2.id";
		$datos = $this->con->consultaRetorno($sql);
		return $datos;
	}
	
	public function add(){
		$sql = "INSERT INTO estudiantes(nombre,edad,promedio,imagen,id_secciones,fecha)
		VALUES('{$this->nombre}','{$this->edad}','{$this->promedio}','{$this->imagen}','{$this->id_secciones}',NOW())";

		$this->con->consultaSimple($sql);
	}

	public function delete(){
		$sql = "DELETE FROM estudiantes WHERE id = '{$this->id}'";
		$this->con->consultaSimple($sql);
	}

	public function edit(){
		$sql = "UPDATE estudiantes SET nombre = '{$this->nombre}', edad = '{$this->edad}', promedio = '{$this->promedio}', id_secciones = '{$this->id_secciones}' WHERE id = '{$this->id}'";
		$this->con->consultaSimple($sql);
	}

	public function view(){
		$sql = "SELECT t1.* , t2.nombre as nombre_seccion FROM estudiantes t1 INNER JOIN secciones t2 ON t1.id_secciones = t2.id WHERE t1.id = '{$this->id}'";
		$datos = $this->con->consultaRetorno($sql);
//Send array
		$row = mysqli_fetch_assoc($datos);
		return $row;
	}


}

?>