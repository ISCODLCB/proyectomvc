<?php namespace Models; 
//Nombres de espacio nombre de la carrpeta
class Conexion
{
	//variables provadas
	private $datos = array(
		"host" => "localhost",
		"user" => "root",
		"pass" => "",
		"db" => "proyecto"
		);

	private $con;
//metod construct
	public function __construct(){
	//Clases gloables \ barra invertida
		$this->con = new \mysqli($this->datos['host'], $this->datos['user'], $this->datos['pass'], $this->datos['db']);
	}

	public function consultaSimple($sql){
		$this->con->query($sql);
	}

	public function consultaRetorno($sql){
		$datos = $this->con->query($sql);
		return $datos;
	}

}

?>