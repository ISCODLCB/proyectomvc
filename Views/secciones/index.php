
<div class="header"><h1>Listado de secciones</h1></div>
<div class="box-principal col-sm-6 col-sm-offset-3">
	<div class="panel panel-success">
		<div class="panel-heading">
			<h3 class="panel-title">Listado de secciones</h3>
		</div>
		<div class="panel-body">
			<table class="table table-striped">
				<tr>
					<th>Id</th>
					<th>Nombre</th>
					<th>Accion</th>
				</tr>
				<?php 
//Guardamos en una variables los datos de la variable instanciada anteriormente
	//$datos = $estudiantes->index();
				while ($row = mysqli_fetch_array($datos)) { ?>

				<tr>
					<td><?php echo $row['id'];?></td>
					<td><?php echo $row['nombre'];?></td>
					<td>
						<a class="btn btn-warning" href="<?php echo URL; ?>secciones/editar/<?php echo $row['id']; ?>">Editar</a>
						<a class="btn btn-danger" href="<?php echo URL; ?>secciones/eliminar/<?php echo $row['id']; ?>">Eliminar</a>

					</td>
				</tr>
				<?php }  ?>
			</table>
		</div>
	</div>
</div>