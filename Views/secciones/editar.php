
<div class="header"><h1>Editar secciones</h1></div>
<div class="box-principal">
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Editar seccion <?php echo $datos['nombre']; ?></h3>
		</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-10">
					<form action="" method="POST" enctype="multipart/form-data">
						<div class="form-group">
							<label for="exampleInputEmail1">Nombre de la seccion</label>
							<input type="text" class="form-control" value="<?php echo $datos['nombre']; ?>" id="exampleInputEmail1" name="nombre" required>
						</div>
						<input type="hidden" value="<?php echo $datos['id']; ?>" name="id" required>
						<div class="form-group">
							<button type="submit" class="btn btn-success">Editar</button>
							<button type="reset" class="btn btn-warning">Borrar</button>
						</div>
					</form>
				</div>
				<div class="col-md-1"></div>
			</div>
		</div>
	</div>
</div>