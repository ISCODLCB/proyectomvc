<?php namespace Views;

//objeto que contenga esta clase
$template = new Template();

class Template{
	public function __construct(){

		?>

		<!DOCTYPE html>
		<html lang="es">
		<head>
			<meta charset="UTF-8">
			<title>Administracion de estudiantes | leafTech</title>
			<link rel="stylesheet" type="text/css" href="<?php echo URL; ?>Views/template/css/bootstrap.min.css">
			<link rel="stylesheet" type="text/css" href="<?php echo URL; ?>Views/template/css/estudiantes-listar.css">
		</head>
		<body>

			<nav class="navbar navbar-default">
				<div class="container-fluid">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="#">Administracion de alumnos</a>
					</div>

					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav">
							<li><a href="<?php echo URL;?>">Inicio<span class="sr-only">(current)</span></a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Estudiantes <span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li><a href="<?php echo URL;?>estudiantes">Listado</a></li>
									<li role="separator" class="divider"></li>
									<li><a href="<?php echo URL;?>estudiantes/agregar">Agregar</a></li>
								</ul>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Secciones <span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li><a href="<?php echo URL;?>secciones">Listado</a></li>
									<li role="separator" class="divider"></li>
									<li><a href="<?php echo URL;?>secciones/agregar">Agregar</a></li>
								</ul>
							</li>

						</ul>
						<form class="navbar-form navbar-left">
							<div class="form-group">
								<input type="text" class="form-control" placeholder="Search">
							</div>
							<button type="submit" class="btn btn-default">Submit</button>
						</form>
						<ul class="nav navbar-nav navbar-right">
							<li><a href="#">Link</a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li><a href="#">Action</a></li>
									<li><a href="#">Another action</a></li>
									<li><a href="#">Something else here</a></li>
									<li role="separator" class="divider"></li>
									<li><a href="#">Separated link</a></li>
								</ul>
							</li>
						</ul>
					</div><!-- /.navbar-collapse -->
				</div><!-- /.container-fluid -->
			</nav>


			<?php 

		}

		public function __destruct(){
			?>

			<!-- Jquery & Bootstrap JavaScript -->
			<script type="text/javascript" src="<?php echo URL; ?>Views/template/js/jquery.js"></script>
			<script type="text/javascript" src="<?php echo URL; ?>Views/template/js/bootstrap.min.js"></script>
		</body>
		</html>


		<?php 

	}
} 

?>