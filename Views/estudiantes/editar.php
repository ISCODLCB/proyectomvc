
<?php
//echo $datos[1]['nombre'];
$secciones = $estudiantes->listarSecciones(); ?>
<div class="header"><h1>Editar estudiante <?php echo $datos['nombre']; ?> </h1></div>
<div class="box-principal">
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Formulario editar <?php echo $datos['nombre']; ?></h3>
		</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-md-3">
					<div class="panel panel-default">
						<div class="panel-body">
							<img class="img-responsive" src="<?php echo URL;?>Views/template/imagenes/avatars/<?php echo $datos['imagen']; ?>">
						</div>						
					</div>
				</div>
				<div class="col-md-9">
					<form action="" method="POST" enctype="multipart/form-data">
						<div class="form-group">
							<label for="exampleInputEmail1">Nombre del estudiante</label>
							<input type="text" class="form-control" value="<?php echo $datos['nombre']; ?>" id="exampleInputEmail1" name="nombre" required>
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Edad</label>
							<input type="number" class="form-control" value="<?php echo $datos['edad']; ?>" id="exampleInputEmail1" name="edad" required>
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Promedio</label>
							<input type="number" class="form-control" value="<?php echo $datos['promedio']; ?>" id="exampleInputEmail1" name="promedio" required>
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Seccion (<b>Sección Actual: <?php echo $datos['nombre_seccion']; ?></b>)</label>
							<select name="id_secciones" class="form-control">
								<?php while ($row = mysqli_fetch_array($secciones)) { ?>
								<option value="<?php echo $row['id']; ?>"><?php echo $row['nombre']; ?></option>
								<?php } ?>

							<!--	<?php for ($col=0; $col < 3 ; $col++) { ?>
								<option value="<?php echo $datos[1]['id']; ?>"><?php echo $datos[1]['nombre']; ?></option>
								<?php } ?>-->
							</select>
						</div>

						<!--Metodo no convencional-->
						<input type="hidden" value="<?php echo $datos['id']; ?>" name="id" required>
						<div class="form-group">
							<button type="submit" class="btn btn-success">Editar</button>
							<button type="reset" class="btn btn-warning">Borrar</button>
						</div>

					</form>
				</div>
			</div>
		</div>
	</div>
</div>