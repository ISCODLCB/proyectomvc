
<div class="header"><h1>Listado de estudiantes</h1></div>

<div class="box-principal col-sm-6 col-sm-offset-3">
	<div class="panel panel-success">
		<div class="panel-heading">
			<h3 class="panel-title">Listado de estudiantes</h3>
		</div>
		<div class="panel-body">
			<table class="table table-striped">
				<tr>
					<th>Imagen</th>
					<th>Nombre</th>
					<th>Edad</th>
					<th>Nombre seccion</th> 
					<th>Pomedio</th>
					<th>Accion</th>
				</tr>
				<?php 
//Guardamos en una variables los datos de la variable instanciada anteriormente
	//$datos = $estudiantes->index();
				while ($row = mysqli_fetch_array($datos)) { ?>

				<tr>
					<td><img class="img-avatar" src="<?php echo URL; ?>Views/template/imagenes/avatars/<?php echo $row['imagen']; ?>"></td>
					<td><a href="<?php echo URL; ?>estudiantes/ver/<?php echo $row['id']; ?>"><?php echo $row['nombre'];?></a></td>
					<td><?php echo $row['edad'];?></td>
					<td><?php echo $row['nombre_seccion'] ?></td>
					<td><?php echo $row['promedio'];?></td>
					<td>
						<a class="btn btn-warning" href="<?php echo URL; ?>estudiantes/editar/<?php echo $row['id']; ?>">Editar</a>
						<a class="btn btn-danger" href="<?php echo URL; ?>estudiantes/eliminar/<?php echo $row['id']; ?>">Eliminar</a>

					</td>
				</tr>
				<?php }  ?>
			</table>
		</div>
	</div>
</div>