
<div class="header"><h1>Agregar estudiantes</h1></div>
<div class="box-principal">
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Formulario</h3>
		</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-10">
					<form action="" method="POST" enctype="multipart/form-data">
						<div class="form-group">
							<label for="exampleInputEmail1">Nombre del estudiante</label>
							<input type="text" class="form-control" id="exampleInputEmail1" name="nombre" required>
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Edad</label>
							<input type="number" class="form-control" id="exampleInputEmail1" name="edad" required>
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Promedio</label>
							<input type="number" class="form-control" id="exampleInputEmail1" name="promedio" required>
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Seccion</label>
							<select name="id_secciones" class="form-control">
								<?php while ($row = mysqli_fetch_array($datos)) { ?>
									<option value="<?php echo $row['id']; ?>"><?php echo $row['nombre']; ?></option>
								<?php } ?>
							</select>
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Imagen</label>
							<input type="file" class="form-control" id="exampleInputEmail1" name="imagen" id="imagen" required>
						</div>
						<div class="form-group">
							<button type="submit" class="btn btn-success">Registrar</button>
							<button type="reset" class="btn btn-warning">Borrar</button>
						</div>

					</form>
				</div>
				<div class="col-md-1"></div>
			</div>
		</div>
	</div>
</div>