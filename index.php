<?php 
//definimos constantes
//Iniciales del directorio
define('DS', DIRECTORY_SEPARATOR);
//Ruta de archivos que estaremos llmando, url amigables
define('ROOT', realpath(dirname(__FILE__)) . DS);
//Ruta para las vistas url de mi pagina web
define('URL', "http://localhost/proyectoMVC/");
//llamar al autoload y cargar clases
require_once 'Config/Autoload.php';
//Correr autorun
Config\Autoload::run();

//Cargar aqui la vista plantilla antes del enrutador
require_once 'Views/template.php';

Config\Enrutador::run(new Config\Request());

/*
//LLamos a la clase
new Config\Request();

//Objeto de la clase
$est = new Models\Estudiante();
////Insertamos un atributo y contenido
$est->set("id",1);
//Obteneos los datos el array
$datos = $est->view();
print $datos['nombre'];
*/

?>